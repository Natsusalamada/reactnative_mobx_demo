import React, { Component } from 'react';
import { View, FlatList, Text, TouchableOpacity, StyleSheet, Alert, Navigator } from 'react-native';
import {observer,Provider} from 'mobx-react'
import TodoListView from "../components/TodoListView";
import TodoListStore from "../mobx/TodoListStore";

const todoListStore = new TodoListStore()

@observer
export default class TodoListContainer extends Component{

    constructor(props){
        super(props)
    
    }

    render() {
        return (
            <Provider todoListStore = {todoListStore}>

                <TodoListView/>

            </Provider>
        )
    }

}
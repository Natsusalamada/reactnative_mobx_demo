import {observable,action,computed} from 'mobx'
import realm from '../database/mobxDemoDatabase';
import { Alert } from 'react-native';
import { updateTodoList, deleteTodoList, queryAllTodoLists, insertNewTodoList, deleteAllTodoLists } from '../database/mobxDemoDatabase';

export default class TodoListStore {

    @observable modalAddViewVisible = false
    @observable dialog_title = ''
    @observable id = 0
    @observable name = ''
    @observable isAddNew = true
    @observable todoLists = []

    @action showModalView(dialog_title,id,name,isAddNew) {
        this.dialog_title = dialog_title
        this.modalAddViewVisible = true
        this.id = id
        this.name = name
        this.isAddNew = isAddNew
    }

    @action dismissModalView() {
        this.dialog_title = ''
        this.modalAddViewVisible = false
        this.id = 0
        this.name = ''
        this.isAddNew = true
    }

    @action listenerDataChange = () => {
        realm.addListener('change', () => {
            this.reloadData();
        });
    }

    @action reloadData = () => {

        queryAllTodoLists().then((res) => {
            this.todoLists = res
        }).catch((error) => {
            this.todoLists = []
            Alert.alert('Error',error)
        });
        console.log(`reloadData`);
    }

    @action insertTask = (newTodoList) => {
         insertNewTodoList(newTodoList).then(
             this.dismissModalView()
         ).catch((error) => {
                Alert.alert(`Insert new todoList error ${error}`);
            });
    }

    @action updateTask = (todoList) => {
        updateTodoList(todoList).then(
            this.dismissModalView()
        ).catch((error) => {
            Alert.alert(`Update todoList error ${error}`);
        });
    }

    @action deleteTask = (id) => {

        deleteTodoList(id).then().catch(error => {
            Alert.alert(`Failed to delete todoList with id = ${id}, error=${error}`);
        });
    }

    @action deleteAllTask = () => {
         deleteAllTodoLists().then().catch(error => {
            alert(`Delete all TodoLists failed. Error = ${error}`);
        });
    }

}


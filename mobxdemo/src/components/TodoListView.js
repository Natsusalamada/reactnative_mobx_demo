import React, { Component } from 'react';
import { View, FlatList, Text, TouchableOpacity, StyleSheet, Alert, Navigator, Modal } from 'react-native';
import {observer, inject} from 'mobx-react'
import Header from "../ultils/header/Header";
import ModalAddView from './ModalAddView'
import Swipeout from 'react-native-swipeout';

@inject('todoListStore')
@observer
export default class TodoListView extends Component{


    constructor(props){
        super(props)

    }

    componentWillMount() {
        
    }
    

    componentDidMount() {
        this.props.todoListStore.reloadData()
        this.props.todoListStore.listenerDataChange()
    }
    

    render() {
        return (
            <View style = {{flex:1}}>
                {this.renderModal()}
                <Header
                    title = {'To Do List'}
                    hasAddButton={true}
                    hasDeleteAllButton={true}
                    deleteAllTodoLists={ () => {
                        this.props.todoListStore.deleteAllTask()
                    }}
                    showAddTodoList={
                        () => {
                            this.props.todoListStore.showModalView('Add a new TodoList',0,'',true)
                        }
                    }
                />
            </View>
        )
    }

    renderModal = () => {
        return (
            <Modal
                animationType='slide'
                transparent
                visible={this.props.todoListStore.modalAddViewVisible}
                
            >
                <ModalAddView
                    dismiss = {() => {
                        this.props.todoListStore.dismissModalView()
                    }}
                    dialog_title = {this.props.todoListStore.dialog_title}
                    id = {this.props.todoListStore.id}
                    name = {this.props.todoListStore.name}
                    isAddNew = {this.props.todoListStore.isAddNew}
                    insertTask = {(newTodoList) => this.props.todoListStore.insertTask(newTodoList)}
                    updateTask = {(todoList) => this.props.todoListStore.insertTask(todoList)}

                />
            </Modal>
        )
    }

    renderListTask = () => {
        return (
            
            <FlatList
                style={styles.flatList}
                data={this.props.todoListStore.todoLists}
                renderItem={({ item, index }) => <FlatListItem {...item} itemIndex={index}
                updateItem={(id,name) => this.props.todoListStore.showAddTodoList('Update a TodoList',id,name,false)}
                onPressItem={() => {
                        alert(`You pressed item `);
                    }}/>}
                keyExtractor={item => item.id}
            />

        )
    }

}

let FlatListItem = props => {
    const { itemIndex, id, name, creationDate, updateItem, onPressItem } = props;
    showEditModal = () => {
        updateItem(id,name)
    }
    showDeleteConfirmation = () => {
        Alert.alert(
            'Delete',
            'Delete a todoList',
            [
                {
                    text: 'No', onPress: () => { },//Do nothing
                    style: 'cancel'
                },
                {
                    text: 'Yes', onPress: () => {
                        this.props.todoListStore.deleteTask(id)
                    }
                },
            ],
            { cancelable: true }
        );
    };
    return (
        <Swipeout right={[
            {
                text: 'Edit',
                backgroundColor: 'rgb(81,134,237)',
                onPress: showEditModal
            },
            {
                text: 'Delete',
                backgroundColor: 'rgb(217, 80, 64)',
                onPress: showDeleteConfirmation
            }
        ]} autoClose={true}>
            <TouchableOpacity onPress={onPressItem}>
                <View style={{ backgroundColor: itemIndex % 2 == 0 ? 'powderblue' : 'skyblue' }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 18, margin: 10 }}>{name}</Text>
                    <Text style={{ fontSize: 18, margin: 10 }} numberOfLines={2}>{creationDate.toLocaleString()}</Text>
                </View>
            </TouchableOpacity>
        </Swipeout >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    flatList: {
        flex: 1,
        flexDirection: 'column',
    }
});
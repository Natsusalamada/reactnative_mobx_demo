import React, { Component } from 'react';
import {
    StyleSheet, View, Text, TouchableOpacity,
    Platform, Image, TextInput, Dimensions
} from "react-native";

const {width, height} = Dimensions.get('window')
export default class ModalAddView extends Component{

    constructor(props){
        super(props)
        this.state = {
            id: 0,
            name: '',
            isAddNew: true
        };
    }

    componentWillMount() {

        this.setState({
            id: this.props.id,
            name: this.props.name,
            isAddNew: this.props.isAddNew
        })

    }

    render() {
        return (
            <View style = {{flex:1,backgroundColor: 'rgba(73,79,89, 0.5)',alignItems: 'center',justifyContent: 'center'}}>
                <TouchableOpacity style = {{...StyleSheet.absoluteFillObject,white:'100%',height:'100%'}} onPress = {() => this.props.dismiss()} ></TouchableOpacity>
                {this.renderContent()}
            </View>
        )
    }

    renderContent = () => {

        const { dialog_title,insertTask,updateTask } = this.props

        return (
            <View style = {{width:width - 150,height:180}}>
                <View style = {{width:'100%',height:50,justifyContent:'center',alignItems:'center',backgroundColor:'rgb(224, 93, 144)'}}>
                    <Text style = {{fontSize: 22}}>{dialog_title}</Text>
                </View>
                <View style={styles.container}>
                    <TextInput style={styles.textInput} placeholder="Enter TodoList name" autoCorrect={false}
                        onChangeText={(text) => this.setState({ name: text })} value={this.state.name}
                    />
                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity style={styles.button} onPress={() => {
                            if (this.state.name.trim() == "") {
                                alert("Please enter todoList' name");
                                return;
                            }
                            
                            if (this.state.isAddNew == true) {
                                const newTodoList = {
                                    id: Math.floor(Date.now() / 1000),
                                    name: this.state.name,
                                    creationDate: new Date()
                                };
                                insertTask(newTodoList)
                            } else {
                                const todoList = {    
                                    id:  this.state.id,
                                    name: this.state.name,                                        
                                };    
                                updateTask(todoList)
                            }
                            
                        }}>
                            <Text style={styles.textLabel}>Save</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.button} onPress={() => {
                            this.props.dismiss()
                        }}>
                            <Text style={styles.textLabel}>Cancel</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'white'
    },
    textInput: {
        height: 40,
        padding: 10,
        margin: 10,
        borderColor: 'gray',
        borderWidth: 1,
        width: width - 170
    },
    button: {
        backgroundColor: 'steelblue',
        padding: 10,
        marginHorizontal: 2,
        marginBottom: 10,
        width:100,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textLabel: {
        color: 'white',
        fontSize: 18,
    }
});

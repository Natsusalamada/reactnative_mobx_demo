import React, { Component } from 'react';
import {
    StyleSheet, View, Text, TouchableOpacity,
    Platform, Image, Alert
} from "react-native";
import Images from "../../assets/images/Images";

const Header = props => {
    const { title, showAddTodoList, hasAddButton, hasDeleteAllButton, deleteAllTodoLists
        } = props;

    return (
        <View style={[styles.row,styles.center,{backgroundColor:'#42c196',height:Platform.OS === 'ios' ? 120 : 70}]}>
            {hasAddButton && <TouchableOpacity style={[styles.center,styles.left,styles.row]} onPress={showAddTodoList}>
                <Image style={styles.scan_qrcode} source= {Images.addIcon} />
            </TouchableOpacity>}
            <View style={[styles.title, styles.center]}>

                <Text style={styles.titleText}>
                    {title}
                </Text>

            </View>
            
            

            {hasDeleteAllButton && <TouchableOpacity style={[styles.center,styles.right,styles.row]} onPress={
                () => {
                    Alert.alert(
                        'Delete all',
                        'Are you sure you want to delete all todoLists ?',
                        [
                            {
                                text: 'No', onPress: () => { },//Do nothing
                                style: 'cancel'
                            },
                            {
                                text: 'Yes', onPress: () => {
                                    deleteAllTodoLists()
                                }
                            },
                        ],
                        { cancelable: true }
                    );
                }
            }>
                <Image style={styles.scan_qrcode} source= {Images.deleteIcon} />
            </TouchableOpacity>}
            
        </View>
    );
};
const styles = StyleSheet.create({
    row: {
        flexDirection: 'row'
    },
    column: {
        flexDirection: 'column'
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    arrow_back:{
        width:14,
        height:24,
        marginRight: 16,
    },
    menu:{
        width: 20,
        height: 14,
        marginRight: 16,
    },
    scan_qrcode:{
        width:24,
        height:24,
        marginRight: 0,
    },
    container: {
        width: '100%',
    },
    left: {
        flex: 2
    },
    title: {
        flex: 6
    },
    right: {
        flex: 2
    },
    titleText: {
        fontSize: 22,
        // fontWeight: '600',
        // fontStyle: 'normal',
        letterSpacing: 0,
    },
    rightText: {
        color: '#42c196',
        fontWeight: '600',
    },
    searchContainer: {
        height: 36,
        backgroundColor: 'rgba(46,55,72, 0.05)',
        flex: 1,
        borderRadius: 18,
    },
    buttonSearchCancel: {
        marginRight: 16,
        backgroundColor: 'red'
    },
    logo_center:{
        height: 45, 
        width: 39, 
        marginRight:'30%',
    },
    logo_right:{
        height: 45, 
        width: 39, 
        alignSelf: 'flex-end', 
        marginTop:24 ,
        marginRight:24
    }
});
export default Header;
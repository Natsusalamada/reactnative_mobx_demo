import React, { Component } from 'react';
import TodoListContainer from './containers/TodoListContainer';
import {
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';

export default TodoListContainer;